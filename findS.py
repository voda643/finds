import cv2
import numpy as np
from skimage.measure import label, regionprops

image = cv2.imread('task1.png')
image1 = np.mean(image, 2)
np.set_printoptions(threshold=np.inf, linewidth=np.inf)
print(image1)

_, threshold = cv2.threshold(image1, 72, 255, 0)
threshold[image1 < 71] = 1

label_image = label(threshold)
regions = regionprops(label_image)

max_area = 0
for region in regions:
    area = region.filled_area - region.perimeter
    print('Площадь региона {}:'.format(region.label), int(area))
    if area > max_area:
        max_area = area
print("Максимальная площадь:", max_area)
