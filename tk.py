from enum import Enum, auto


class TokenType(Enum):
    TYPE = auto()
    VARIABLE = auto()
    ASSIGN = auto()
    NUMBER = auto()
    SYMBOL = auto()
    AMPERSAND = auto()
    OPERATOR = auto()
    EOF = auto()


class Token:
    def __init__(self, value, token_type):
        self.value = value
        self.token_type = token_type

    def __str__(self):
        return f"{self.token_type.name}('{self.value}')"


class Lexer:
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_char = self.text[self.pos]

    def error(self):
        raise Exception('Invalid character')

    def step(self):
        self.pos += 1
        if self.pos > len(self.text) - 1:
            self.current_char = None
        else:
            self.current_char = self.text[self.pos]

    def skip_whitespace(self):
        while self.current_char != "" and self.current_char.isspace():
            self.step()

    def get_next_token(self):
        while self.current_char:
            if self.current_char.isspace():
                self.skip_whitespace()
                continue

            if self.current_char.isdigit():
                token_value = ''
                while self.current_char and self.current_char.isdigit():
                    token_value += self.current_char
                    self.step()
                return Token(token_value, TokenType.NUMBER)

            if self.current_char.isalpha():
                token_value = ''
                while self.current_char and (self.current_char.isalpha() or self.current_char.isdigit()):
                    token_value += self.current_char
                    self.step()

                if token_value == 'int':
                    return Token(token_value, TokenType.TYPE)
                else:
                    return Token(token_value, TokenType.VARIABLE)

            if self.current_char == '=':
                token_value = self.current_char
                self.step()
                return Token(token_value, TokenType.ASSIGN)

            if self.current_char == '*':
                token_value = self.current_char
                self.step()
                return Token(token_value, TokenType.OPERATOR)

            if self.current_char == '&':
                token_value = self.current_char
                self.step()
                return Token(token_value, TokenType.AMPERSAND)

            if self.current_char == ';':
                token_value = self.current_char
                self.step()
                return Token(token_value, TokenType.SYMBOL)

            self.error()

        return Token("", TokenType.EOF)


text_token = 'int value = 10; int* ptr = &value;'
lexer = Lexer(text_token)

tokens = []
token = lexer.get_next_token()
while token.token_type != TokenType.EOF:
    tokens.append(token)
    token = lexer.get_next_token()

for token in tokens:
    print(token)
